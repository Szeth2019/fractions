# python3 -m venv PythonDataStruc/venv
# source PythonDataStruc/venv/bin/activate
# pip install -r requirements.txt

def gcd(m,n):
		while m%n != 0:
			oldm = m
			oldn = n 

			m = oldn
			n = oldm%oldn

		return n


class Fraction:

	def __init__(self, top, bottom):
		try:
			top = int(top)
			bottom = int(bottom)

			common = gcd(top, bottom)

			self.num = top//common
			self.den = bottom//common

		#Handle the exception
		except ValueError:
			print("Please enter an integer")


	def getNum(self):
		return self.num

	def getDen(self):
		return self.den

	def __str__(self):
		return str(self.num) + "/" + str(self.den)		

	def __add__(self, otherfraction):

		newnum = self.num * otherfraction.den + otherfraction.num * self.den
		newden = self.den * otherfraction.den

		return Fraction(newnum, newden)


	def __sub__(self, otherfraction):

		newnum = self.num * otherfraction.den - otherfraction.num * self.den
		newden = self.den * otherfraction.den

		return Fraction(newnum, newden)

	# I am skipping other fraction operations for now because I am moving on to exceptions


myf = Fraction("a",5)

myf = Fraction(3,5)
print("I ate", myf, "of the pizza.")

f1 = Fraction(1,4)
f2 = Fraction(3,2)

f3 = f1 + f2

print(f3)
print("The numerator is", f3.getNum())
print("The denominator is", f3.getDen())

f1 = Fraction(4,5)
f2 = Fraction(1,2)

f3 = f1 - f2

print(f3)
print("The numerator is", f3.getNum())
print("The denominator is", f3.getDen())




